package id.multipolar.will.learnbasicspring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.multipolar.will.learnbasicspring.data.Bar;

@Configuration
public class BarConfiguration {

	@Bean
	public Bar bar() {
		return new Bar();
	}
	
}
