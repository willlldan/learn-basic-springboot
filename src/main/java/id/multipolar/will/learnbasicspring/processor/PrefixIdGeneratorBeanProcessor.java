package id.multipolar.will.learnbasicspring.processor;

import java.util.UUID;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import id.multipolar.will.learnbasicspring.aware.IdAware;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PrefixIdGeneratorBeanProcessor implements BeanPostProcessor, Ordered{

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		log.info("Prefix Id Generator Processor for Bean {}", beanName);
		if (bean instanceof IdAware) {
			log.info("Prefix Set id Generator for Bean {}", beanName);
			IdAware idAware = (IdAware) bean;
			idAware.setId("WILL-" + idAware.getId());
		}

		return bean;
	}

	@Override
	public int getOrder() {
		return 2;
	}
	
}