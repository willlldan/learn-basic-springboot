package id.multipolar.will.learnbasicspring.factory;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import id.multipolar.will.learnbasicspring.client.PaymentGatewayClient;

@Component("paymentGatewayClient")
public class PaymentGatewatClientFactoryBean implements FactoryBean<PaymentGatewayClient> {

	@Override
	public PaymentGatewayClient getObject() throws Exception {
		PaymentGatewayClient client = new PaymentGatewayClient();
		client.setEndpoint("https://example.com");
		client.setPrivateKey("PRIVATE");
		client.setPublicKey("PUBLIC");
		return client;
	}

	@Override
	public Class<?> getObjectType() {
		// TODO Auto-generated method stub
		return PaymentGatewayClient.class;
	}

}
