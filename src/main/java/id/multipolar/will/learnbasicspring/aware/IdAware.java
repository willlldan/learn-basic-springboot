package id.multipolar.will.learnbasicspring.aware;

public interface IdAware {

	void setId(String id);

	String getId();
	
}
