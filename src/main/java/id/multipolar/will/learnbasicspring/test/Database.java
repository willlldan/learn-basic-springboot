package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.Configuration;

public class Database {

	private static Database database;
	
	private Database() {
		
	}
	
	public static Database getInstance() {
		if(database == null) {
			database = new Database();
		}
		
		return database;
	}
}
