package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.multipolar.will.learnbasicspring.data.Foo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class BeanConfiguration {

	@Bean
	public Foo foo() {
		Foo foo = new Foo();
		log.info("Create new foo");
		return foo;
	}
}
