package id.multipolar.will.learnbasicspring.test;

import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import id.multipolar.will.learnbasicspring.data.Bar;
import id.multipolar.will.learnbasicspring.data.Foo;
import id.multipolar.will.learnbasicspring.scope.DoubletonScope;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ScopeConfiguration {

	@Bean
	@Scope("prototype")
	public Foo foo() {
		log.info("Create new foo");
		return new Foo();
	}
	
	@Bean
	public CustomScopeConfigurer customScopeConfigurer() {
		CustomScopeConfigurer configurer = new CustomScopeConfigurer();
		configurer.addScope("doubleton", new DoubletonScope());
		return configurer;
	}
	
	@Bean
	@Scope("doubleton")
	public Bar bar() {
		log.info("Create new bar");
		return new Bar();
	}
}
