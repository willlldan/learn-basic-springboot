package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import id.multipolar.will.learnbasicspring.data.MultiFoo;

@Configuration
@ComponentScan(basePackages = {
		"id.multipolar.will.learnbasicspring.service",
		"id.multipolar.will.learnbasicspring.repository",
		"id.multipolar.will.learnbasicspring.configuration"
})
@Import(MultiFoo.class)
public class ComponentConfiguration {

}
