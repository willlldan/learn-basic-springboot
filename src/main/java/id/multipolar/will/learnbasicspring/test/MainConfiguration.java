package id.multipolar.will.learnbasicspring.test;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import id.multipolar.will.learnbasicspring.configuration.BarConfiguration;
import id.multipolar.will.learnbasicspring.configuration.FooConfiguration;

@Configuration
@Import({
	FooConfiguration.class,
	BarConfiguration.class
})
public class MainConfiguration {

}
