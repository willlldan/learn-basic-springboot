package id.multipolar.will.learnbasicspring.listener;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import id.multipolar.will.learnbasicspring.event.LoginSuccessEvent;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UserListener {

	
	@EventListener(classes= LoginSuccessEvent.class)
	public void onLoginSuccessEvent(LoginSuccessEvent event) {
		log.info("Success login for user {}", event.getUser());
	}
	
	@EventListener(classes= LoginSuccessEvent.class)
	public void onLoginSuccessEvent2(LoginSuccessEvent event) {
		log.info("Success login for user {}", event.getUser());
	}
	
	@EventListener(classes= LoginSuccessEvent.class)
	public void onLoginSuccessEvent3(LoginSuccessEvent event) {
		log.info("Success login for user {}", event.getUser());
	}
	
}
