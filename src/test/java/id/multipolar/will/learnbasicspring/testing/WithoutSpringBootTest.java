package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.application.FooApplication;

public class WithoutSpringBootTest {

	private ConfigurableApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(FooApplication.class);
		context.registerShutdownHook();
	}
	
	@Test
	void test() {
		
	}
	
}
