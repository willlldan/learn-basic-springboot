package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.data.Foo;
import id.multipolar.will.learnbasicspring.test.DuplicateBeanConfiguration;

public class DuplicateTest {

	@Test
	void testDuplicate() {
		ApplicationContext context = new AnnotationConfigApplicationContext(DuplicateBeanConfiguration.class);
		
	}
	
	void getBean() {
		ApplicationContext context = new AnnotationConfigApplicationContext(DuplicateBeanConfiguration.class);
		
		Foo foo1 = context.getBean("foo1", Foo.class);
		Foo foo2 = context.getBean("foo2", Foo.class);
		
		Assertions.assertNotSame(foo1, foo2);
	}
	
}
