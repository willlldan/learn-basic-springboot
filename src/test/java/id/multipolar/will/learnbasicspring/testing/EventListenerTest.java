package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import id.multipolar.will.learnbasicspring.listener.LoginAgainSuccessListener;
import id.multipolar.will.learnbasicspring.listener.LoginSuccessListener;
import id.multipolar.will.learnbasicspring.listener.UserListener;
import id.multipolar.will.learnbasicspring.service.UserService;

public class EventListenerTest {

	@Configuration
	@Import({
		UserService.class,
		LoginSuccessListener.class,
		LoginAgainSuccessListener.class,
		UserListener.class
	})
	public static class TestConfiguration {
		
	}
	
	private ConfigurableApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(TestConfiguration.class);
		context.registerShutdownHook();
	}
	
	@Test
	void testEven() {
		UserService service = context.getBean(UserService.class);
		
		service.login("will", "will");
		service.login("will", "rahasia");
		service.login("will", "1234");
		
	}
	
}
