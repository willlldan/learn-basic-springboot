package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.test.CyclicConfiguration;

public class CyclicTest {

	
	@Test
	void testCyclic() {
		Assertions.assertThrows(Throwable.class, () -> {
			ApplicationContext context = new AnnotationConfigApplicationContext(CyclicConfiguration.class);
		});
	}
	
}
