package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.client.PaymentGatewayClient;
import id.multipolar.will.learnbasicspring.configuration.FactoryConfiguration;

public class FactoryTest {

	private ConfigurableApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(FactoryConfiguration.class);
		context.registerShutdownHook();
	}
	
	@Test
	void testFactory() {
		PaymentGatewayClient client = context.getBean(PaymentGatewayClient.class);
		
		Assertions.assertNotNull(client);
		Assertions.assertEquals(client.getEndpoint(), "https://example.com");
		Assertions.assertEquals(client.getPrivateKey(), "PRIVATE");
		Assertions.assertEquals(client.getPublicKey(), "PUBLIC");
	}
	
}
