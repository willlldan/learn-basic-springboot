package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.configuration.OptionalConfiguration;
import id.multipolar.will.learnbasicspring.data.Foo;
import id.multipolar.will.learnbasicspring.data.FooBar;

public class OptionalTest {

	private ConfigurableApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(OptionalConfiguration.class);
		context.registerShutdownHook();
	}
	
	@Test
	void testOption() {
		Foo foo = context.getBean(Foo.class);
		FooBar fooBar = context.getBean(FooBar.class);
		
		Assertions.assertNull(fooBar.getBar());
		Assertions.assertSame(fooBar.getFoo(), foo);
				
	}
	
}
