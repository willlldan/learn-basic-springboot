package id.multipolar.will.learnbasicspring.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import id.multipolar.will.learnbasicspring.configuration.InheritanceConfiguration;
import id.multipolar.will.learnbasicspring.service.MerchantService;
import id.multipolar.will.learnbasicspring.service.MerchantServiceImpl;

public class InheritenceTest {

	private ConfigurableApplicationContext context;
	
	@BeforeEach
	void setUp() {
		context = new AnnotationConfigApplicationContext(InheritanceConfiguration.class);
		context.registerShutdownHook();
	}
	
	@Test
	void testHeritance() {
		MerchantService merchantService = context.getBean(MerchantService.class);
		MerchantServiceImpl merchantServiceImpl = context.getBean(MerchantServiceImpl.class);
		
		Assertions.assertSame(merchantService, merchantServiceImpl);
	}
	
}
